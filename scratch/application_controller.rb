class ApplicationController < ActionController::Base
  # before_action :load_object, only: [:show, :edit, :update, :destroy]
  # before_action :load_collection, only: :index

  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token, if: Proc.new { request.format.json? }

  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  private

  # ?

  # def record_not_found(exc)
  #   raise exc unless request.format.json?
  #   render json: { message: exc.message }, status: 404
  # end

  # def load_object
  #   @object = controller_name.classify.constantize.find(params[:id])
  #   instance_variable_set("@#{controller_name.singularize.to_sym}", @object)
  # end

  # def load_collection
  #   @collection = controller_name.classify.constantize.all
  #   instance_variable_set("@#{controller_name.to_sym}", @collection)
  # end

end
