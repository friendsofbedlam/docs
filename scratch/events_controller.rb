class EventsController < ApplicationController
  respond_to :html, :json
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token, if: Proc.new { request.format.json? }

  # rescue_from StandardError, with: :something_broke

  # def something_broke(e)
  #   raise e unless request.format.json?
  #   render json: e.message
  # end

  def index
    @events = Event.all
  end

  def show
    # To make /events/1 redirect to /productions/1
    # redirect_to send("#{@event.type.underscore}_path")
    @event = @event.decorate
    respond_with(@event.decorate)
  end

  def new
    @event = Event.new
  end

  def edit
  end

  def create
    @event = Event.new(event_params)
    @event.save
    respond_with(@event)
  end

  def update
    @event.update(event_params)
    respond_with(@event)
  end

  def destroy
    @event.destroy
    respond_with(@event)
  end

  private

  def set_event
    @event = Event.eager_load(:participants, participants: [:participant_type, :entity]).find(params[:id])
  end

  def event_params
    params.require(:event).permit(:type, :name, :datetime, :description)
  end
end
