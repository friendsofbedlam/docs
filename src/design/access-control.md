# Access Control

The application uses simple [role-based access control](https://en.wikipedia.org/wiki/Role-based_access_control) to decide who can do what. Each user can have zero or more roles (a user with zero roles is differentiated from one that is not logged in).

Anywhere in the system that requires a privileged user will examine the roles and decide if the user is authorized based on this; we make use of [pundit] to write policies to manage this.

There are no explicit "permissions", only roles; this has a number of side effects:

* the authorization code is much simpler
* application code must decide which role(s) it requires
* users cannot be assign individual permissions

The intent here is to keep things simple and flexible.

The roles are as follows:

* Administrator (`:admin`)
* Editor (`:editor`)

which can only be added to by making changes to the application code because, again, code has to decide which roles it requires. There is no user-facing way to add additional roles: with no part of the software examining them, this would be pointless.

## Code Example

Consider the following policy:

```ruby
class UserPolicy < ApplicationPolicy
  def update?
    user.has_role?(:admin) || user.id == record.id
  end
end
```

This describes a restriction on who can update a user record: any user who is an admin, as well as that user. We could enforce that as follows:

```ruby
class UsersController < ApplicationController
  # snip
  def update
    @user = User.find(params[:id])
    authorize @user, :update?
  end
end
```

(although note pundit has support for automatically authorizing actions in a resourceful way so in practice this example is slightly contrived)

[pundit]: https://github.com/elabs/pundit
