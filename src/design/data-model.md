# The Database

## Models

### Event

The `Event` model has the following fields/types:

* `id:int`
* `type:string`
* `name:string`
* `datetime:datetime`
* `description:text`

STI: `Production`

### Entity

The `Entity` model has the following fields/types:

* `id:int`
* `type:string`
* `name:string`
* `description:text`

STI: `Person`, `Organisation`

### Participant

The `Participant` model has the following fields/types:

* `event_id:int`
* `entity_id:int`
* `participant_type_id:int`
* `name:string`

### ParticipantType

The `ParticipantType` model has the following fields/types:

* `id:int`
* `name:string`
* `is_nameable:boolean`
* `entity_types:array`

### Attachment

### AttachmentType

### Copyright

### CopyrightType

# Prototype

First, build up some application scaffolding:

```shell
rails new archive-prototype
cd archive-prototype
rake db:create
rails generate scaffold Event type:string name:string datetime:datetime description:text
rails generate scaffold Entity type:string name:string description:text
rails generate scaffold ParticipantType name:string is_nameable:boolean entity_types:text
rails generate scaffold Participant event:references entity:references participant_type:references name:string
```
*Note that we would probably alter the migration for participants to remove the `id` field and add a compsite key*

Next we need to add some additional information to some of the newly created models (add these lines inside the classes):

```ruby
# app/models/participant_type.rb
serialize :entity_types, Array
has_many :participants
```

And additionally create some extra classes for our STI models:

```ruby
# app/models/person.rb
class Person < Entity
  def self.model_name
    Entity.model_name
  end
end
```

```ruby
# app/models/organisation.rb
class Organisation < Entity
  def self.model_name
    Entity.model_name
  end
end
```

```ruby
# app/models/production.rb
class Production < Event
  def self.model_name
    Production.model_name
  end
end
```

Now we have a horrible, but working implementation of our desired data model. Go forth and change everything.

(We probably would avoid using the same controller for all STI subclasses?)

Due to the constraints of scaffolding, we can only create ParticipantTypes from the console. Do that now:

```ruby
ParticipantType.create! name: 'Director', is_nameable: false, entity_types: ["Person"]
ParticipantType.create! name: 'Producer', is_nameable: false, entity_types: ["Person", "Organisation"]
ParticipantType.create! name: 'Actor', is_nameable: true, entity_types: ["Person"]
```

After some fiddling with `responders`, `draper` and excessive `eager_load` calls, things are looking quite sane.
