# Questions

A list of design-related questions that need answered:

* Who can create accounts? Anybody? Do they have to be approved or is email address acceptable
* Who can edit stuff? Any registered user? Do we have a moderation queue? Or a "suggest an edit" feature?
* How will known 3rd-party application integrate? Can they make edits? Import events?
* What is our recommendation re embedding/linking?
* How do we do approx. date searching? There are various techniques but it depends on how we anticipate the data being searched
